import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { MyAccountComponent } from './components/my-account/my-account.component';
import { MyProfileComponent } from './components/my-profile/my-profile.component';
import { VodListComponent } from './components/vod-list/vod-list.component';
import { ViewVodComponent } from './components/view-vod/view-vod.component';
import { TutorsComponent } from './components/tutors/tutors.component';
import { GoLiveComponent } from './components/go-live/go-live.component';

const routes: Routes = [
  {
    path:'',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'myAccount',
    component: MyAccountComponent
  },
  {
    path: 'myProfile',
    component: MyProfileComponent
  },
  {
    path: 'vodList',
    component: VodListComponent
  },
  {
    path: 'viewVod/:id',
    component: ViewVodComponent
  },
  {
    path: 'tutors',
    component: TutorsComponent
  },
  {
    path: 'goLive',
    component: GoLiveComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
