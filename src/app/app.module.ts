import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { Router } from '@angular/router';
import { DataFilterPipe } from './components/vod-list/data-filter.pipe';

import { MaterialModule } from './material.module';

import { VgCoreModule } from 'videogular2/core';
import { VgControlsModule } from 'videogular2/controls';
import { VgStreamingModule } from 'videogular2/streaming';
import { VgOverlayPlayModule } from 'videogular2/overlay-play';
import { VgBufferingModule } from 'videogular2/buffering';


import 'hammerjs';

import { LoginService } from './services/login.service';
import { UserService } from './services/user.service';
import { VodService } from './services/vod.service';
import { UploadThumbnailService } from './services/upload-thumbnail.service';
import { UploadVideoService } from './services/upload-video.service';
import { RandomVodListService } from './services/random-vod-list.service';
import { PaymentService } from './services/payment.service';
import { UploadCvService } from './services/upload-cv.service';
import { GoLiveService } from './services/go-live.service';
import { SubscribeService } from './services/subscribe.service';
import { PlaylistService } from './services/playlist.service';

import { MyAvigator } from './interface/my-avigator';

import { HomeComponent } from './components/home/home.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { MyAccountComponent } from './components/my-account/my-account.component';
import { MyProfileComponent } from './components/my-profile/my-profile.component';
import { VodListComponent } from './components/vod-list/vod-list.component';
import { ViewVodComponent } from './components/view-vod/view-vod.component';
import { GoLiveComponent } from './components/go-live/go-live.component';
import { TutorsComponent } from './components/tutors/tutors.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavBarComponent,
    MyAccountComponent,
    MyProfileComponent,
    VodListComponent,
    DataFilterPipe,
    ViewVodComponent,
    GoLiveComponent,
    TutorsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    HttpClientModule,
    BrowserAnimationsModule,
    VgCoreModule,
    VgControlsModule,
    VgStreamingModule,
    VgOverlayPlayModule,
    VgBufferingModule
  ],
  providers: [
    LoginService,
    UserService,
    VodService,
    UploadThumbnailService,
    UploadVideoService,
    RandomVodListService,
    PaymentService,
    UploadCvService,
    GoLiveService,
    SubscribeService,
    PlaylistService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
