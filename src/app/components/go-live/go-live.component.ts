import { Component, OnInit, ViewChild } from '@angular/core';
import { GoLiveService } from '../../services/go-live.service';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { RandomVodListService } from '../../services/random-vod-list.service';
import { VgAPI } from 'videogular2/core';

export interface IMedia {
  title: string;
  src: string;
  type: string;
}

@Component({
  selector: 'app-go-live',
  templateUrl: './go-live.component.html',
  styleUrls: ['./go-live.component.css']
})

export class GoLiveComponent implements OnInit {

  private user: User = new User();

  private cameraDevices: String;
  private audioDevices: String;

  private nrOfVods: number = 10;
  private randomVodList: any;
  private i: number;

  playlist2: Array<IMedia>;
  playlist: Array<IMedia> = [
    {
      title: 'Pale Blue Dot',
      src: 'http://static.videogular.com/assets/videos/videogular.mp4',
      type: 'video/mp4'
    },
    {
      title: 'Big Buck Bunny',
      src: 'http://static.videogular.com/assets/videos/big_buck_bunny_720p_h264.mov',
      type: 'video/mp4'
    },
    {
      title: 'Elephants Dream',
      src: 'http://static.videogular.com/assets/videos/elephants-dream.mp4',
      type: 'video/mp4'
    }
  ];

  currentIndex = 0;
  //currentItem: IMedia = this.playlist2[this.currentIndex];

  private currentItem: IMedia;
  api: VgAPI;


  constructor(private goLiveService: GoLiveService,
    private userService: UserService,
    private randomVodService: RandomVodListService) { }

  onDiscoverVideoDevices() {

    this.userService.getCurrentUser().subscribe(
      res => {

        console.log(res);
        this.user.id = res["id"];
        this.user.firstName = res["firstName"];
        this.user.lastName = res["lastName"];
        this.user.username = res["username"];
        this.user.email = res["email"];
        this.user.userPaymentList = res["userPaymentList"];

        console.log(this.user.id);

        this.goLiveService.discoverVideoDevices(this.user.id).subscribe(
          data1 => {
            console.log(data1);
          },
          error1 => {
            console.log(error1["error"]["text"]);
            this.cameraDevices = error1["error"]["text"];
          }
        )
      },
      error => {
        console.log(error);
      }
    )


  }

  onDiscoverAudioDevices() {
    this.userService.getCurrentUser().subscribe(
      res => {

        console.log(res);
        this.user.id = res["id"];
        this.user.firstName = res["firstName"];
        this.user.lastName = res["lastName"];
        this.user.username = res["username"];
        this.user.email = res["email"];
        this.user.userPaymentList = res["userPaymentList"];

        console.log(this.user.id);

        this.goLiveService.discoverAudioDevices(this.user.id).subscribe(
          data1 => {
            console.log(data1);
          },
          error1 => {
            console.log(error1["error"]["text"]);
            this.audioDevices = error1["error"]["text"];
          }
        )
      },
      error => {
        console.log(error);
      }
    )

  }

  onStartRecording() {
    this.goLiveService.startRecording().subscribe(
      res => {
        console.log(res);
      },
      error => {
        console.log(error);
      }
    )
  }

  onStopRecording() {
    this.userService.getCurrentUser().subscribe(
      res => {

        console.log(res);
        this.user.id = res["id"];
        this.user.firstName = res["firstName"];
        this.user.lastName = res["lastName"];
        this.user.username = res["username"];
        this.user.email = res["email"];
        this.user.userPaymentList = res["userPaymentList"];

        console.log(this.user.id);

        this.goLiveService.stopRecording(this.user.id).subscribe(
          data1 => {
            console.log(data1);
          },
          error1 => {
            console.log(error1["error"]["text"]);
          }
        )
      },
      error => {
        console.log(error);
      }
    )
  }

  getRandomVodList() {
    this.randomVodService.randomVodList(this.nrOfVods).subscribe(
      res => {
        console.log(res);
        console.log("random vod list");
        for (let i = 0; i < 10; i++) {
        }

        this.playlist2 = [
          {
            title: res[0]["name"],
            src: 'http://localhost:8181/vod/videos/' + res[0]["id"] + '.m3u8',
            type: 'video/*'
          },
          {
            title: res[1]["name"],
            src: 'http://localhost:8181/vod/videos/' + res[1]["id"] + '.m3u8',
            type: 'video/*'
          },
          {
            title: res[2]["name"],
            src: 'http://localhost:8181/vod/videos/' + res[2]["id"] + '.m3u8',
            type: 'video/*'
          },
          {
            title: res[3]["name"],
            src: 'http://localhost:8181/vod/videos/' + res[3]["id"] + '.m3u8',
            type: 'video/*'
          }, {
            title: res[4]["name"],
            src: 'http://localhost:8181/vod/videos/' + res[4]["id"] + '.m3u8',
            type: 'video/*'
          },
          {
            title: res[5]["name"],
            src: 'http://localhost:8181/vod/videos/' + res[5]["id"] + '.m3u8',
            type: 'video/*'
          }, {
            title: res[6]["name"],
            src: 'http://localhost:8181/vod/videos/' + res[6]["id"] + '.m3u8',
            type: 'video/*'
          }, {
            title: res[7]["name"],
            src: 'http://localhost:8181/vod/videos/' + res[7]["id"] + '.m3u8',
            type: 'video/*'
          },
          {
            title: res[8]["name"],
            src: 'http://localhost:8181/vod/videos/' + res[8]["id"] + '.m3u8',
            type: 'video/*'
          },
          {
            title: res[9]["name"],
            src: 'http://localhost:8181/vod/videos/' + res[9]["id"] + '.m3u8',
            type: 'video/*'
          }

        ]





        this.currentItem = this.playlist2[this.currentIndex];
        //this.randomVodList=res;
        //this.randomVodList=res;
        // console.log(this.randomVodList.id);

      },
      error => {
        console.log(error);
      }
    )

  }

  ngOnInit() {
    this.getRandomVodList();
  }

  onPlayerReady(api: VgAPI) {
    this.api = api;

    this.api.getDefaultMedia().subscriptions.loadedMetadata.subscribe(this.playVideo.bind(this));
    this.api.getDefaultMedia().subscriptions.ended.subscribe(this.nextVideo.bind(this));
  }

  nextVideo() {
    this.currentIndex++;

    if (this.currentIndex = this.nrOfVods-1) {
      this.currentIndex = 0;
    }

    this.currentItem = this.playlist2[this.currentIndex];
  }

  playVideo() {
    this.api.play();
  }

  onClickPlaylistItem(item: IMedia, index: number) {
    this.currentIndex = index;
    this.currentItem = item;
  }

}
