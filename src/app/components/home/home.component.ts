import { Component, OnInit } from '@angular/core';
import { RandomVodListService } from '../../services/random-vod-list.service';
import {ActivatedRoute, Router} from '@angular/router';
import { Vod } from '../../models/vod';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private randomVodList: any;
  private nrOfVods = 6;
  private selectedVod= new Vod;

  constructor(private randomVodListService: RandomVodListService,
    private route: ActivatedRoute,
    private router: Router) { }

    onSelect(vod: Vod) {
      this.selectedVod = vod;
      this.router.navigate(['/viewVod', this.selectedVod.id]);
    }

  getRandomVodList() {
    this.randomVodListService.randomVodList(this.nrOfVods).subscribe(
      res => {
        console.log(res);
        console.log("random vod list");
        this.randomVodList=res;
      },
      error => {
        console.log(error);
      }
    )
    
  }

  onBack() {
    this.router.navigate[('/vodList')];
  }
  
  ngOnInit() {
    this.getRandomVodList();
  }

}
