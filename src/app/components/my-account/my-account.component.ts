import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';
import { UserService } from '../../services/user.service';
import { AppConst } from '../../constants/app-const';
import { UploadCvService } from '../../services/upload-cv.service';



@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {

  private serverPath = AppConst.serverPath;
  private loginError: boolean =false;
  private loggedIn = false;
  private credential = {'username':'', 'password':''};

  private emailSent: boolean =false;
  private usernameExists: boolean = false;
  private emailExists: boolean =false;
  private username: string;
  private email: string;

  private emailNotExists: boolean=false;
  private forgetPasswordEmailSent: boolean;
  private recoverEmail: string;

  private obj:any;

  private cvUploaded: boolean=false;
  private selectedNewAccountTab: number;
  
  
  constructor(
    private loginService: LoginService,
    private userService: UserService,
    private uploadCVService: UploadCvService,
    private router: Router
  ) { }

  selectedNewAccountChange(val: number) {
    this.selectedNewAccountTab = val;
  }

  onLogin(){
    this.loginService.sendCredentials(this.credential.username, this.credential.password).subscribe(
      res => {
        localStorage.setItem('xAuthToken', res["token"]);
        this.loggedIn=true;
        location.reload();
      },
      error => {
        console.log(error);
        this.loggedIn=false;
        this.loginError=true;
      }
    );
  }

  onNewStudentAccount() {
    this.usernameExists =false;
    this.emailExists=false;
    this.emailSent=false;
    this.selectedNewAccountTab=0;

    this.userService.newUser(this.username, this.email).subscribe(
      data => {
        console.log("DATA");
        console.log(data);
        this.emailSent=true;
      },
      error => {
        console.log(error["error"]);
        let errorMessage= error["error"];
        if(errorMessage=="usernameExists") this.usernameExists=true;
        if(errorMessage=="emailExists") this.emailExists=true;

      }
    );
  }

  onNewTutorAccount() {
    this.usernameExists =false;
    this.emailExists=false;
    this.emailSent=false;
    this.selectedNewAccountTab=1;

    this.userService.newTutor(this.username, this.email).subscribe(
      data => {
        this.uploadCVService.uploadCV(this.username);
        this.cvUploaded=true;
        console.log("DATA");
        console.log(data);
        this.emailSent=true;
      },
      error => {
        console.log(error["error"]);
        let errorMessage= error["error"];
        if(errorMessage=="usernameExists") this.usernameExists=true;
        if(errorMessage=="emailExists") this.emailExists=true;
      }
    );
  }

  onForgetPassword(){
    this.forgetPasswordEmailSent=false;
    this.emailNotExists=false;

    this.userService.retrievePassword(this.recoverEmail).subscribe(
      data => {
        console.log("RES");
        console.log(data);
        this.forgetPasswordEmailSent=true;
      },
      error => {
        console.log(error);
        console.log(error["error"]);
        let errorMessage= error["error"];
        if(errorMessage=="Email not found") this.emailNotExists=true;
      }
    );
  }

  ngOnInit() {
    this.selectedNewAccountTab=1;
    this.loginService.checkSession().subscribe(
      res => {
        this.loggedIn=true;
        this.router.navigate(['home']);
      },
      error => {
        this.loggedIn=false;
      }
    );
  }

}
