import { Component, OnInit } from '@angular/core';
import { AppConst } from '../../constants/app-const';
import { User } from '../../models/user';
import { UserPayment } from '../../models/user-payment';
import { UserBilling } from '../../models/user-billing';
import { UserService } from '../../services/user.service';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';
import { Vod } from '../../models/vod';
import { VodService } from '../../services/vod.service';
import { UploadThumbnailService } from '../../services/upload-thumbnail.service';
import { UploadVideoService } from '../../services/upload-video.service';
import { PaymentService } from '../../services/payment.service';
import { GoLiveService } from '../../services/go-live.service';
import { SubscribeService } from '../../services/subscribe.service';
import { PlaylistService } from '../../services/playlist.service';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {

  private serverPath = AppConst.serverPath;
  private dataFetched = false;
  private loginError: boolean;
  private loggedIn: boolean;
  private credential = { 'username': '', 'password': '' };

  private user: User = new User();
  private userO: User = new User();
  private updateSuccess: boolean;
  private newPassword: string;
  private incorrectPassword: boolean;
  private currentPassword: string;

  private userPayment: UserPayment = new UserPayment();
  private userBilling: UserBilling = new UserBilling();
  private userPaymentList: any;
  private defaultPaymentSet: boolean;
  private defaultUserPaymentId: number;

  private selectedProfileTab: number = 0;
  private selectedBillingTab: number = 0;
  private selectedPlaylistTab: number = 0;

  private vodAdded: boolean;
  private vod: Vod = new Vod();

  private userRole: string;
  private isStudent: boolean;

  private stateList: string[] = [];

  private likedVideoList: any;
  private watchLaterList: any;
  private followingList: any;

  private selectedVod: Vod;


  constructor(
    private userService: UserService,
    private loginService: LoginService,
    private vodService: VodService,
    private paymentService: PaymentService,
    private uploadThumbnail: UploadThumbnailService,
    private uploadVideo: UploadVideoService,
    private goLiveService: GoLiveService,
    private subscribeService: SubscribeService,
    private playListService: PlaylistService,
    private router: Router
  ) { }

  selectedBillingChange(val: number) {
    this.selectedBillingTab = val;
  }

  selectedPlaylistChange(val: number) {
    this.selectedPlaylistTab = val;
  }

  onSelect(vod: Vod) {
    this.selectedVod = vod;
    this.router.navigate(['/viewVod', this.selectedVod.id]);
  }

  onUpdateUserInfo() {
    this.userService.updateUserInfo(this.user, this.newPassword, this.currentPassword).subscribe(
      res => {
        this.updateSuccess = true;
      },
      error => {
        console.log(error["error"]);
        let errorMessage = error["error"];
        if (errorMessage == "Incorrect current password!") this.incorrectPassword = true;
      }
    )
  }

  getCurrentUser() {
    this.userService.getCurrentUser().subscribe(
      res => {
        this.user.id = res["id"];
        this.user.firstName = res["firstName"];
        this.user.lastName = res["lastName"];
        this.user.username = res["username"];
        this.user.email = res["email"];
        this.user.userPaymentList = res["userPaymentList"];

        console.log(this.user.id);
        this.vodService.getLikedVideoList(this.user.id).subscribe(
          data => {
            console.log("Inside get liked vod list");
            console.log(data);
            this.likedVideoList = data;
          },
          error => {
            console.log(error);
          }
        )

        this.playListService.getWatchLater(this.user.id).subscribe(
          data2=>{
            this.watchLaterList=data2;
          },
          error2=>{
            console.log(error2);
          }
        )

        this.subscribeService.followingList(this.user.id).subscribe(
          data1 => {
            console.log("Inside get following");
            console.log(data1);
            this.followingList = data1;
          },
          error => {
            console.log(error);
          }
        )


        this.userService.getCurrentUserRole(this.user.id).subscribe(
          data => {
            console.log(data);
          },
          error2 => {
            this.user.userRole = error2["error"]["text"];
            this.onUserStatus();
          }
        )

        this.userPaymentList = this.user.userPaymentList;

        for (let index in this.userPaymentList) {
          if (this.userPaymentList[index].defaultPayment) {
            this.defaultUserPaymentId = this.userPaymentList[index];
            break;
          }
        }

        console.log(this.user.userPaymentList);
        this.dataFetched = true;
      },
      error => {
        console.log(error);
      }
    )
  }

  onUserStatus() {
    if (this.user.userRole == "Student") {
      this.isStudent = true;
    } else {
      this.isStudent = false;
    }
  }

  onAddNewVideo() {
    this.vodService.addVod(this.vod).subscribe(
      res => {
        this.uploadThumbnail.uploadThumbnail(res["id"]);
        this.uploadVideo.uploadVideo(res["id"]);
        this.vodAdded = true;
        this.vod = new Vod();
        this.userService.getCurrentUser().subscribe(
          data => {
            console.log("On submit ------->");
            console.log(data);
            console.log(this.userO);

            this.userO.id = data["id"];
            this.userO.firstName = data["firstName"];
            this.userO.lastName = data["lastName"];
            this.userO.username = data["username"];
            this.userO.email = data["email"];

            this.vod.user = this.userO;

            console.log(this.userO.username);
          },
          error => {
            console.log(error);
          }
        )
        //this.router.navigate(['/home']);
      },
      error => {
        console.log("error");
      }
    )
  }


  onNewPayment() {
    this.paymentService.newPayment(this.userPayment).subscribe(
      res => {
        this.getCurrentUser();
        this.selectedBillingTab = 0;
      },
      error => {
        console.log(error.text());
      }
    );
  }

  onUpdatePayment(payment: UserPayment) {
    this.userPayment = payment;
    this.userBilling = payment.userBilling;
    this.selectedBillingTab = 1;
  }

  onRemovePayment(id: number) {
    this.paymentService.removePayment(id).subscribe(
      res => {
        this.getCurrentUser();
      },
      error => {
        console.log(error);
      }
    );
  }



  setDefaultPayment(id: number) {
    this.defaultPaymentSet = false;
    this.paymentService.setDefaultPayment(this.defaultUserPaymentId).subscribe(
      res => {
        this.getCurrentUser();
        this.defaultPaymentSet = true;
      },
      error => {
        console.log(error);
      }
    );
  }

  ngOnInit() {

    this.selectedPlaylistTab = 0;

    this.loginService.checkSession().subscribe(
      res => {
        this.loggedIn = true;
      },
      error => {
        this.loggedIn = false;
        console.log("inactive session");
        this.router.navigate(['home']);
      }
    )

    this.vodAdded = false;
    this.userService.getCurrentUser().subscribe(
      res => {
        this.userO.id = res["id"];
        this.userO.firstName = res["firstName"];
        this.userO.lastName = res["lastName"];
        this.userO.username = res["username"];
        this.userO.userPaymentList = res["userPaymentList"];
        this.vod.user = this.user;
      },
      error => {
        console.log(error);
      }
    )

    this.getCurrentUser();

    for (let s in AppConst.roStates) {
      this.stateList.push(s);
    }

    this.userBilling.userBillingState = "";
    this.userPayment.type = "";
    this.userPayment.expiryMonth = "";
    this.userPayment.expiryYear = "";
    this.userPayment.userBilling = this.userBilling;
    this.defaultPaymentSet = false;

  }



}
