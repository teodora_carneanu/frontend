import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';


@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  constructor(private loginService: LoginService) { }

  private loggedIn = false;

  logout(){
    this.loginService.logout().subscribe(
      res => {
        localStorage.clear();
        location.reload();
      }, 
      error => {
        console.log(error);
      }
    )
  }

  onSearchByTitle(){
    
  }

  ngOnInit() {
    this.loginService.checkSession().subscribe(
      res => {
        this.loggedIn =true;
      },
      error => {
        this.loggedIn=false;
      }
    );
  }

}
