import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { SubscribeService } from '../../services/subscribe.service';

@Component({
  selector: 'app-tutors',
  templateUrl: './tutors.component.html',
  styleUrls: ['./tutors.component.css']
})
export class TutorsComponent implements OnInit {

  constructor(private userService: UserService,
              private subscribeService: SubscribeService) { }

  private tutorList: any;
  private user: User= new User();

  onSubscribe(user_id: number){
    console.log("The ID of the TUTOR");
    console.log(user_id);

    this.userService.getCurrentUser().subscribe(
      res=>{
        this.user.id = res["id"];
        this.user.firstName = res["firstName"];
        this.user.lastName = res["lastName"];
        this.user.username = res["username"];
        this.user.email = res["email"];
        this.user.userPaymentList = res["userPaymentList"];

        this.subscribeService.subscribeToUser(user_id, this.user).subscribe(
          data => {
            console.log(data);
          }, 
          error=>{
            console.log(error);
          }
        )
        
        
      }, 
      error=>{
        console.log(error);
      }
    )
  }

  ngOnInit() {
    this.userService.tutorList().subscribe(
      res=>{
        this.tutorList=res;
      },
      error=>{
        console.log(error);
      }
    )
  }

}
