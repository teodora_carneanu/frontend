import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewVodComponent } from './view-vod.component';

describe('ViewVodComponent', () => {
  let component: ViewVodComponent;
  let fixture: ComponentFixture<ViewVodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewVodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewVodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
