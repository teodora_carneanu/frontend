import { Component, OnInit } from '@angular/core';
import { VodService } from '../../services/vod.service';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { Vod } from '../../models/vod';
import { VgPlayer, VgAPI } from 'videogular2/core';
import { User } from 'src/app/models/user';
import { UserService } from '../../services/user.service';
import { PlaylistService } from '../../services/playlist.service';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}

@Component({
  selector: 'app-view-vod',
  templateUrl: './view-vod.component.html',
  styleUrls: ['./view-vod.component.css']
})
export class ViewVodComponent implements OnInit {

  private vod: Vod = new Vod;
  private vodId: number;
  private vodUrl: string;
  private pathToVod = "http://localhost:8181/vod/videos/";
  private api;

  private user: User = new User();

  private liked: boolean;

  private tiles: Tile[];

  private filterQuery: string;

  constructor(private vodService: VodService, private playlistService: PlaylistService, private userService: UserService, private route: ActivatedRoute, private router: Router) { }

  getCurrentUser(){
    this.userService.getCurrentUser().subscribe(
      res=>{
        this.user.id = res["id"];
        this.user.firstName = res["firstName"];
        this.user.lastName = res["lastName"];
        this.user.username = res["username"];
        this.user.email = res["email"];
        this.user.userPaymentList = res["userPaymentList"];
      
        
      }, 
      error=>{
        console.log(error);
      }
    )
  }

  onLike() {

    this.route.params.forEach((params: Params) => {
      this.vodId = Number.parseInt(params['id']);
    })

    this.vodService.likedVideo(this.vodId, this.user).subscribe(
      res => {
        location.reload();
      },
      error => {
        console.log(error);
      }
    )
  }

  onDislike() {
    this.route.params.forEach((params: Params) => {
      this.vodId = Number.parseInt(params['id']);
    })

    this.vodService.dislikedVideo(this.vodId, this.user).subscribe(
      res => {
        location.reload();
      },
      error => {
        console.log(error);
      }
    )
  }

  onBack() {
    this.router.navigate[('/vodList')];
  }

  /*   
  onPlayerReady(api: VgAPI) {
      this.api = api;
      this.api.getDefaultMedia().subscriptions.loadedMetadata.subscribe(
        this.playVideo.bind(this)
      );
    } */

  onPlayerReady(api: VgAPI) {
    this.api = api;
  }

  playVideo() {
    this.api.play();
  }

  onAddPlaylist(){
    console.log(this.filterQuery);
    console.log(this.vodId);
    this.getCurrentUser;
    this.user.id;
    this.playlistService.addPlaylist(this.vodId, this.user, this.filterQuery).subscribe(
      res=>{
        console.log(res);
      },
      error=>{
        console.log(error);
      }
    )
    
  }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      this.vodId = Number.parseInt(params['id']);
    })

    this.vodService.getVod(this.vodId).subscribe(
      res => {
        console.log(res["id"]);
        this.vod.id = res["id"];
        this.vod.name = res["name"];
        this.vod.description = res["description"];
        this.vod.category = res["category"];
        this.vod.language = res["language"];
        this.vod.tags = res["tags"];
        this.vod.duration = res["duration"];

        this.vodUrl = this.pathToVod + this.vod.id + '.mp4';

        this.tiles = [
          {text: "Title: " + this.vod.name, cols: 3, rows: 1, color: 'lightgray'},
          {text: "Description: "+this.vod.description , cols: 1, rows: 3, color: 'lightgray'},
          {text: "Category: "+ this.vod.category, cols: 1, rows: 1, color: 'lightgray'},
          {text: "Duration: "+this.vod.duration+ " seconds", cols: 1, rows: 1, color: 'lightgray'},
          {text: "Publication Date: "+this.vod.publicationDate, cols: 1, rows: 1, color: 'lightgray'},
          {text: "Language: "+this.vod.language, cols: 1, rows: 1, color: 'lightgray'},
          {text: "Tags: "+this.vod.tags, cols: 2, rows: 1, color: 'lightgray'},
      
        ]

      },
      error => {
        console.log(error);
      }
    );


    this.userService.getCurrentUser().subscribe(
      res => {
        console.log("On init in view vod setting current user ------->");
        console.log(res);
        this.user.id = res["id"];
        this.user.firstName = res["firstName"];
        this.user.lastName = res["lastName"];
        this.user.username = res["username"];
        this.user.userPaymentList = res["userPaymentList"];

        console.log("On init before isLiked  ------->");

        console.log(this.vodId);
        console.log(this.user.id);

        this.vodService.isLiked(this.vodId, this.user).subscribe(
          data => {
            if (JSON.stringify(data) == "true") {
              this.liked = true;
            } else if (JSON.stringify(data) == "false") {
              this.liked = false;
            } else {
              this.liked = false;
            }
          },
          error => {
          }
        )
      },
      error => {
        console.log(error);
      }
    )


  }

}
