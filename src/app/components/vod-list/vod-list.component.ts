import { Component, OnInit } from '@angular/core';
import { Vod } from '../../models/vod';
import { VodService } from '../../services/vod.service';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AppConst } from '../../constants/app-const';
import { DataFilterPipe } from './data-filter.pipe';

@Component({
  selector: 'app-vod-list',
  templateUrl: './vod-list.component.html',
  styleUrls: ['./vod-list.component.css']
})
export class VodListComponent implements OnInit {

  displayedColumns: string[] = ['name', 'description', 'publicationDate', 'tags', 'language', 'category'];

  public filterQuery = "";
  public rowsOnPage = 5;

  private selectedVod: Vod;
  private vodList: any;
  private serverPath = AppConst.serverPath;

  constructor(
    private vodService: VodService,
    private router: Router,
    private http: HttpClient,
    private route: ActivatedRoute
  ) { }

  onSelect(vod: Vod) {
    this.selectedVod = vod;
    this.router.navigate(['/viewVod', this.selectedVod.id]);
  }

  getVodList() {
    this.vodService.getVodList().subscribe(
      data => {
        this.vodList = data;
      },
      error => {
        console.log(error);
      }
    )
  }

  onSearcyByTitle() {
    console.log("Pressed");
    console.log(this.filterQuery);
    if (this.filterQuery == null) {
      this.getVodList();
    } else {
      console.log("Printing something");
      this.vodService.searchByTitle(this.filterQuery).subscribe(
        res => {
          this.vodList = res;
        },
        error => {
          console.log("Search not working");
          console.log("error");
        }
      )
    }
  }

  ngOnInit() {
    this.onSearcyByTitle();
  }

}
