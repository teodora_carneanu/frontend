import { UserPayment } from './user-payment';

export class User {
    public id: number;
    public username: string;
    public password: string;
    public firstName: string;
    public lastName: string;
    public email: string;
    public enabled: boolean;
    public userPaymentList: UserPayment[];
    public userRole: string
}
