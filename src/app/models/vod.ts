import { User } from './user';

export class Vod {
    public id: number;

    public name: string;
    public description: string;
    public publicationDate: string;
    public language: string;
    public category: string;
    public tags: string;
    public duration: number;

    public user: any;

    public active: boolean;
}
