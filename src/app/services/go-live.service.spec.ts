import { TestBed } from '@angular/core/testing';

import { GoLiveService } from './go-live.service';

describe('GoLiveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GoLiveService = TestBed.get(GoLiveService);
    expect(service).toBeTruthy();
  });
});
