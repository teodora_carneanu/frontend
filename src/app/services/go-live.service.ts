import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConst } from '../constants/app-const';


@Injectable({
  providedIn: 'root'
})
export class GoLiveService {

  private serverPath: string = AppConst.serverPath;

  constructor(private http: HttpClient) { }

  discoverVideoDevices(id: number){
    let url=this.serverPath+'/goLive/discoverVideoDevices?id='+id;
    let tokenHeaders = new HttpHeaders({
      'Content-Type' : 'application/json',
      'x-auth-token' : localStorage.getItem("xAuthToken")
    });

    return this.http.get(url, {headers: tokenHeaders});
    
  }

  discoverAudioDevices(id: number){
    let url=this.serverPath+'/goLive/discoverAudioDevices?id='+id;
    let tokenHeaders = new HttpHeaders({
      'Content-Type' : 'application/json',
      'x-auth-token' : localStorage.getItem("xAuthToken")
    });

    return this.http.get(url, {headers: tokenHeaders});
    
  }

  startRecording(){
    let url=this.serverPath+'/goLive/startRecording';
    let tokenHeaders = new HttpHeaders({
      'Content-Type' : 'application/json',
      'x-auth-token' : localStorage.getItem("xAuthToken")
    });

    return this.http.post(url,'', {headers: tokenHeaders});
    
  }

  stopRecording(id: number){
    let url=this.serverPath+'/goLive/stopRecording?id='+id;
    let tokenHeaders = new HttpHeaders({
      'Content-Type' : 'application/json',
      'x-auth-token' : localStorage.getItem("xAuthToken")
    });

    return this.http.post(url, '', {headers: tokenHeaders});
    
  }
}
