import { Injectable } from '@angular/core';
import { AppConst } from '../constants/app-const';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user';
@Injectable({
  providedIn: 'root'
})
export class PlaylistService {

  private serverPath: string = AppConst.serverPath;

  constructor(private http: HttpClient) { }

  addPlaylist(vod_id: number, user: User, name: String){
    let url=this.serverPath+'/playlist/add?id='+vod_id+'&name='+name;
    let tokenHeaders = new HttpHeaders({
      'Content-Type' : 'application/json',
      'x-auth-token' : localStorage.getItem("xAuthToken")
    });

    return this.http.post(url, JSON.stringify(user), {headers: tokenHeaders});
    
  }

  getWatchLater(user_id: number){
    let url=this.serverPath+'/playlist/playListByUser?id='+user_id;
    let tokenHeaders = new HttpHeaders({
      'Content-Type' : 'application/json',
      'x-auth-token' : localStorage.getItem("xAuthToken")
    });

    return this.http.get(url, {headers: tokenHeaders});
    
  }
}
