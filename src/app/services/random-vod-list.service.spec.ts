import { TestBed } from '@angular/core/testing';

import { RandomVodListService } from './random-vod-list.service';

describe('RandomVodListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RandomVodListService = TestBed.get(RandomVodListService);
    expect(service).toBeTruthy();
  });
});
