import { Injectable } from '@angular/core';
import { AppConst } from '../constants/app-const';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RandomVodListService {

  private serverPath=AppConst.serverPath;

  constructor(private http: HttpClient) { }

  randomVodList(n: number){
    let url=this.serverPath+'/vod/randomVodList/'+n;
    let headers = new HttpHeaders({
      'Content-Type' : 'application/json'
    });

    return this.http.get(url, {headers: headers});
  }
}
