import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConst } from '../constants/app-const';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class SubscribeService {

  private serverPath: string = AppConst.serverPath;

  constructor(private http: HttpClient) { }

  subscribeToUser(user_id: number, user: User){
    let url=this.serverPath+'/subscribe/add?id='+user_id;
    let tokenHeaders = new HttpHeaders({
      'Content-Type' : 'application/json',
      'x-auth-token' : localStorage.getItem("xAuthToken")
    });

    return this.http.post(url, JSON.stringify(user), {headers: tokenHeaders});
    
  }

  followingList(user_id:number){
    let url=this.serverPath+'/subscribe/getFollowing?id='+user_id;
    let tokenHeaders = new HttpHeaders({
      'Content-Type' : 'application/json',
      'x-auth-token' : localStorage.getItem("xAuthToken")
    });

    return this.http.get(url, {headers: tokenHeaders});
    
  }

}
