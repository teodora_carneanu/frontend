import { TestBed } from '@angular/core/testing';

import { UploadCvService } from './upload-cv.service';

describe('UploadCvService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UploadCvService = TestBed.get(UploadCvService);
    expect(service).toBeTruthy();
  });
});
