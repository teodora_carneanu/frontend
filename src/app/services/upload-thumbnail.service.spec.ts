import { TestBed } from '@angular/core/testing';

import { UploadThumbnailService } from './upload-thumbnail.service';

describe('UploadThumbnailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UploadThumbnailService = TestBed.get(UploadThumbnailService);
    expect(service).toBeTruthy();
  });
});
