import { TestBed } from '@angular/core/testing';

import { VodService } from './vod.service';

describe('VodService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VodService = TestBed.get(VodService);
    expect(service).toBeTruthy();
  });
});
