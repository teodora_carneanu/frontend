import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Vod } from '../models/vod';
import { User } from '../models/user';
import { AppConst } from '../constants/app-const';

@Injectable({
  providedIn: 'root'
})
export class VodService {

  private serverPath: string = AppConst.serverPath;

  constructor( private http: HttpClient ) { }

  getVodList(){
    let url = this.serverPath+'/vod/vodList';
    let headers = new HttpHeaders({
      'Content-Type' : 'application/json'
    });

    return this.http.get(url, {headers : headers});    
  }

  getVod(id: number){
    let url = this.serverPath+"/vod/" +id;
    let headers = new HttpHeaders({
      'Content-Type' : 'application/json'
    });

    return this.http.get(url, {headers: headers});
  }

  searchVod(keyword: string){
    let url=this.serverPath + "/vod/searchVod";

    let headers = new HttpHeaders({
      'Content-Type' : 'application/json'
    });

    return this.http.post(url, keyword, {headers: headers});
  }

  addVod(vod: Vod) {
    let url=this.serverPath+'/vod/add';
    let tokenHeaders = new HttpHeaders({
      'Content-Type' : 'application/json',
      'x-auth-token' : localStorage.getItem("xAuthToken")
    });

    return this.http.post(url, JSON.stringify(vod), {headers: tokenHeaders});
  }

  likedVideo(vod_id: number, user: User){
    let url=this.serverPath+'/likedVideo/add?id='+vod_id;
    let tokenHeaders = new HttpHeaders({
      'Content-Type' : 'application/json',
      'x-auth-token' : localStorage.getItem("xAuthToken")
    });

    return this.http.post(url, JSON.stringify(user), {headers: tokenHeaders});
    
  }

  dislikedVideo(vod_id: number, user: User){
    let url=this.serverPath+'/likedVideo/remove?id='+vod_id;
    let tokenHeaders = new HttpHeaders({
      'Content-Type' : 'application/json',
      'x-auth-token' : localStorage.getItem("xAuthToken")
    });

    return this.http.post(url, JSON.stringify(user), {headers: tokenHeaders});
    
  }

  isLiked(vod_id: number, user: User){
    let url=this.serverPath+'/likedVideo/isLiked?id='+vod_id;
    let tokenHeaders = new HttpHeaders({
      'Content-Type' : 'application/json',
      'x-auth-token' : localStorage.getItem("xAuthToken")
    });

    return this.http.post(url, JSON.stringify(user), {headers: tokenHeaders});
    
  }

  getLikedVideoList(user_id: number){
    let url=this.serverPath+'/likedVideo/likedVideosList?id='+user_id;
    let tokenHeaders = new HttpHeaders({
      'Content-Type' : 'application/json',
      'x-auth-token' : localStorage.getItem("xAuthToken")
    });

    return this.http.get(url, {headers: tokenHeaders});
    
  }

  searchByTitle(title: string){
    let url =this.serverPath+'/vod/blurrySearch?title='+title;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-auth-token': localStorage.getItem('xAuthToken')
    });

    return this.http.get(url, {headers: headers});
  }
}
